package myApplication.controller;

import myApplication.entity.Design;
import myApplication.service.DesignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class DesignController {

    @Autowired
    DesignService designService;

    @PostMapping("/addDesign")
    public @ResponseBody Design addDesign(@RequestBody @Valid Design design){
        return designService.addDesign(design);
    }

    @DeleteMapping("/deleteDesign/{id}")
    public void deleteDesign(@PathVariable Integer id){
        designService.deleteDesignById(id);
    }

    @PutMapping("/updateDesign/{id}")
    public @ResponseBody Design updateDesign(@PathVariable Integer id, @RequestBody Design design){
        return designService.updateDesign(id, design);
    }

}
