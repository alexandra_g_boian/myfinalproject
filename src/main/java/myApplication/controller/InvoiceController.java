package myApplication.controller;

import myApplication.entity.Invoice;
import myApplication.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class InvoiceController {

    @Autowired
    InvoiceService invoiceService;

    @PostMapping("/addInvoice")
    public @ResponseBody Invoice addInvoice (@RequestBody Invoice invoice){
        return invoiceService.addInvoice(invoice);
    }

    @GetMapping("/getAll")
    public @ResponseBody Iterable<Invoice> getAllInvoices(){
        return invoiceService.getAllInvoices();
    }

    @PutMapping("/updateInvoice/{id}")
    public @ResponseBody Invoice updateInvoiceById(@PathVariable Integer id, @RequestBody Invoice invoice){
        invoiceService.updateInvoiceById(id, invoice);
                return invoice;
    }

    @DeleteMapping("/deleteInvoice/{id}")
    public String deleteInvoiceById(@PathVariable Integer id){
        invoiceService.deleteInvoiceById(id);
        return("Deleted");

    }

}
