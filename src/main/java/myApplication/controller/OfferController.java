package myApplication.controller;

import myApplication.entity.Offer;
import myApplication.entity.ProductsToBeAdded;
import myApplication.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class OfferController {

    @Autowired
    OfferService offerService;

    @PostMapping("/addOffer")
    public @ResponseBody
    Offer addOffer(@RequestBody Offer offer) {

        return offerService.addOffer(offer);
    }

    @PostMapping("/addProductToOffer")
    public @ResponseBody
    Offer addProductToOffer(@RequestBody ProductsToBeAdded product) {
        return offerService.addProductToOffer(product);
    }

    @PostMapping("/saveOffer")
    public @ResponseBody
    Offer saveProduct() {
        return offerService.saveOffer();
    }


    @GetMapping("/getAllOffers/{status}")
    public @ResponseBody
    List<Offer> getAllOffersByStatus(@PathVariable String status, @RequestBody Offer offer) {
        return offerService.getAllOffersByStatus(status, offer);
    }

    @PutMapping("/updateOffer/{id}")
    public @ResponseBody
    Offer updateOffer(@PathVariable Integer id, @RequestBody Offer offer) {
        return offerService.updateOffer(id, offer);
    }

}
