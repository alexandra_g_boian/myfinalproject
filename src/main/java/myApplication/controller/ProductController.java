package myApplication.controller;

import myApplication.entity.Product;
import myApplication.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping("/addProduct")
    public @ResponseBody
    Product addProduct(@RequestBody Product product) {
        return productService.addProduct(product);
    }

    @DeleteMapping("deleteProduct/{id}")
    public @ResponseBody
    String deleteProduct(@PathVariable Integer id) {

        productService.deleteProduct(id);
        return "Deleted!";
    }

    @PutMapping("/updateProduct/{id}")
    public @ResponseBody
    Product updateProduct(@PathVariable Integer id, @RequestBody Product product) {
        productService.updateProduct(id, product);
        return product;
    }

    @GetMapping("/getAllProducts")
    public @ResponseBody List<Product> getAllProducts(Product product) {
        return productService.getAllProducts(product);
    }

}
