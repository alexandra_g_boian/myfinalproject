package myApplication.controller;

import myApplication.entity.ProductsToBeAdded;
import myApplication.service.ProductsToBeAddedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ProductsToBeAddedController {

    @Autowired
    ProductsToBeAddedService productsToBeAddedService;

    @PostMapping("/addProducts")
    public @ResponseBody ProductsToBeAdded addProducts(@RequestBody ProductsToBeAdded products){
        return productsToBeAddedService.addProducts(products);
    }
}
