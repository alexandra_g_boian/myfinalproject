package myApplication.dao;


import myApplication.entity.Design;
import org.springframework.data.repository.CrudRepository;

public interface DesignRepository extends CrudRepository <Design, Integer> {
}
