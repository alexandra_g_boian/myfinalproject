package myApplication.dao;

import myApplication.entity.Invoice;
import org.springframework.data.repository.CrudRepository;

public interface InvoiceRepository extends CrudRepository <Invoice, Integer> {
}
