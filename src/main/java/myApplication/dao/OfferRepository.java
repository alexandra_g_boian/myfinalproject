package myApplication.dao;

import myApplication.entity.Offer;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface OfferRepository extends CrudRepository<Offer, Integer> {

    default Offer updateOffer(Integer id, Offer offer){
        Optional<Offer> offerToUpdate = findById(id);
        if (offerToUpdate.isPresent()){
            Offer offerWithIdFound = offerToUpdate.get();
            offerWithIdFound.setClientName(offer.getClientName());
            offerWithIdFound.setClientAddress(offer.getClientAddress());
            offerWithIdFound.setStatus(offer.getStatus());
            offerWithIdFound.setProducts(offer.getProducts());
            return save(offerWithIdFound);
        }
        return save(offer);
    }
}
