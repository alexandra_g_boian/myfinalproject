package myApplication.dao;

import myApplication.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProductRepository extends CrudRepository<Product, Integer> {

    default Product updateProduct(Integer id, Product product) {
        Optional<Product> productToUpdate = findById(id);
        if (productToUpdate.isPresent()){
            Product productWithIdFound = productToUpdate.get();
            productWithIdFound.setName(product.getName());
            productWithIdFound.setPrice(product.getPrice());
            productWithIdFound.setQuantity(product.getQuantity());
            return save(productWithIdFound);
        }
        return save(product);
    }
}
