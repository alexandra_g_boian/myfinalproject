package myApplication.dao;

import myApplication.entity.ProductsToBeAdded;
import org.springframework.data.repository.CrudRepository;

public interface ProductsToBeAddedRepository extends CrudRepository<ProductsToBeAdded, Integer> {
}
