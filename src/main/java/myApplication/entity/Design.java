package myApplication.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Date;

@Entity
public class Design {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer designID;

    private String name;

    private Date dueDate;

    public Design(Integer id,String name, Date dueDate) {
        this.designID =id;
        this.name = name;
        this.dueDate = dueDate;
    }

    public Design() {
    }

    public Integer getDesignID() {
        return designID;
    }

    public void setDesignID(Integer designID) {
        this.designID = designID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

}
