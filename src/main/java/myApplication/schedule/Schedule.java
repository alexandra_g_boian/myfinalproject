package myApplication.schedule;

import myApplication.entity.Offer;
import myApplication.service.EmailService;
import myApplication.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class Schedule {
    @Autowired
    OfferService offerService;

    Offer offer = new Offer();

    @Scheduled(cron = "0 00 12 * * ?")
    @Async
    public void scheduleTaskForUnknownStatus() {

        List<Offer> offerList = offerService.getAllOffersByStatus("unknown", offer);
        for (Offer offer : offerList) {
            EmailService.emailSender("alexandra.g.boian@gmail.com", "Offers with status unknown!", "Please check the list below");

        }
    }

    @Scheduled(cron = "0 00 12 * * ?")
    @Async
    public void scheduleTaskForAccountancy() {
        List<Offer> offerList = offerService.getAllOffersByStatus("approved", offer);

        for (Offer offer : offerList) {
            EmailService.emailSender("alexandra.g.boian@gmail.com", "Offers with status approved!", "Please get the invoice ready within 48h");

        }
    }

    @Scheduled(cron = "0 40 12 * * ?")
    @Async
    public void changeStatusAutomatically() {
        List<Offer> offerList = offerService.getAllOffersByStatus("unknown", offer);
        List<Offer> tempList = new ArrayList<>();
        for (Offer offer : offerList) {
            Date startDate = new Date(offer.getEntryDate());
            Date currentDate = new Date(System.currentTimeMillis());
            long diffInMillies = Math.abs(currentDate.getTime() - startDate.getTime());
            long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
            if (diff >= 1) {
                offer.setStatus("pending");
                offerService.updateOffer(offer.getId(), offer);
                tempList.add(offer);
            }
        }
        String mailToSend = "The status has been changed from 'unknown' to 'pending', for the following offer id's: " + mailWithNewPendingOffers(tempList);
        EmailService.emailSender("alexandra.g.boian@gmail.com", "New Pending Offers", mailToSend);
    }

    private String mailWithNewPendingOffers(List<Offer> tempList) {
        StringBuilder sb = new StringBuilder();
        sb.append(" ");
        for (Offer offer: tempList) {
            sb.append(offer.getId());
            sb.append(" ");
        }
        return sb.toString();
    }

}
