package myApplication.service;

import myApplication.controller.UserController;
import myApplication.dao.DesignRepository;
import myApplication.entity.Design;
import myApplication.entity.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DesignService {

    final DesignRepository designRepository;
    final UserController userController;

    public DesignService(DesignRepository designRepository, UserController userController) {
        this.designRepository = designRepository;
        this.userController = userController;
    }

    public Design addDesign(Design design) {
        Design designSaved = designRepository.save(design);
        if (designSaved != null) {

            Iterable<User> users = userController.getUsersByRole("prodTeamMember");
            for (User user : users) {
                EmailService.emailSender(user.getEmail(), "Attention!", "New design has been added!");
            }


        } else {
            System.out.println("Something went wrong!");
        }
        return designSaved;
    }

    public void deleteDesignById(Integer id) {
        designRepository.deleteById(id);
    }

    public Design updateDesign(Integer id, Design newDesign) {
        Optional<Design> designToBeUpdated = designRepository.findById(id);
        if (designToBeUpdated.isPresent()) {
            Design designWithIdFound = designToBeUpdated.get();
            designWithIdFound.setName(newDesign.getName());
            designWithIdFound.setDueDate(newDesign.getDueDate());
            return designRepository.save(designWithIdFound);
        }
        return designRepository.save(newDesign);
    }

}
