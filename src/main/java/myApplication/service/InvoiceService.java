package myApplication.service;

import myApplication.dao.InvoiceRepository;
import myApplication.entity.Invoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvoiceService {

    @Autowired
    InvoiceRepository invoiceRepository;

    public Invoice addInvoice(Invoice invoice) {
        invoiceRepository.save(invoice);
        return invoice;
    }

    public Iterable<Invoice> getAllInvoices() {
        return invoiceRepository.findAll();
    }

    public Invoice updateInvoiceById(Integer id, Invoice newInvoice) {
        Optional<Invoice> invoiceToBeUpdated = invoiceRepository.findById(id);
        if (invoiceToBeUpdated.isPresent()){
            Invoice invoiceWithIdFound = invoiceToBeUpdated.get();
            invoiceWithIdFound.setInvoiceNumber(newInvoice.getInvoiceNumber());
            invoiceWithIdFound.setIssueDate(newInvoice.getIssueDate());
            invoiceWithIdFound.setClientName(newInvoice.getClientName());
            invoiceWithIdFound.setItem(newInvoice.getItem());
            invoiceWithIdFound.setQuantity(newInvoice.getQuantity());
            invoiceWithIdFound.setPrice(newInvoice.getPrice());
            invoiceRepository.save(invoiceWithIdFound);
            return invoiceWithIdFound;
        }


       invoiceRepository.save(newInvoice);
        return newInvoice;
    }

    public void deleteInvoiceById(Integer id) {
        invoiceRepository.deleteById(id);
    }
}
