package myApplication.service;

import myApplication.dao.OfferRepository;
import myApplication.dao.ProductsToBeAddedRepository;
import myApplication.entity.Offer;
import myApplication.entity.ProductsToBeAdded;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferService {

    @Autowired
    OfferRepository offerRepository;

    @Autowired
    ProductsToBeAddedRepository productsToBeAddedRepository;

    Offer temporaryOffer = new Offer();
    ProductsToBeAdded productsToBeAdded = new ProductsToBeAdded();

    public Offer addOffer(Offer offer) {
        temporaryOffer = offer;
        return temporaryOffer;
    }

    public Offer addProductToOffer(ProductsToBeAdded product) {

        temporaryOffer.getProducts().add(product);
        return temporaryOffer;
    }

    public Offer saveOffer() {
        temporaryOffer.setStatus("unknown");
        temporaryOffer.setEntryDate(System.currentTimeMillis());

        productsToBeAddedRepository.saveAll(temporaryOffer.getProducts());
        return offerRepository.save(temporaryOffer);
    }

    public List<Offer> getAllOffersByStatus(String status, Offer offer) {
        return (List<Offer>) offerRepository.findAll();
    }

    public Offer updateOffer(Integer id, Offer offer) {
        return offerRepository.updateOffer(id, offer);
    }

}
