package myApplication.service;

import myApplication.dao.ProductRepository;
import myApplication.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public Product addProduct(Product product) {
        productRepository.save(product);
        return product;
    }

    public void deleteProduct(Integer id) {
        productRepository.deleteById(id);
    }


    public Product updateProduct(Integer id, Product product) {
        return productRepository.updateProduct(id, product);
    }

    public List<Product> getAllProducts(Product product) {
        return (List<Product>) productRepository.findAll();
    }
}
