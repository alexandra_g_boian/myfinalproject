package myApplication.service;

import myApplication.dao.ProductRepository;
import myApplication.dao.ProductsToBeAddedRepository;
import myApplication.entity.Product;
import myApplication.entity.ProductsToBeAdded;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductsToBeAddedService {

    @Autowired
    ProductsToBeAddedRepository productsToBeAddedRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    OfferService offerService;

    public ProductsToBeAdded addProducts(ProductsToBeAdded products) {
        Optional<Product> productsToBeAdded = productRepository.findById(products.getId_prod());

        if (productsToBeAdded.isPresent()) {
            Product productWithIdFound = productsToBeAdded.get();
            if (products.getQuantity() <= productWithIdFound.getQuantity()) {
                offerService.addProductToOffer(products);
                return products;
            } else {
                return null;
            }

        }
        return null;
    }

    public ProductsToBeAdded saveProducts(ProductsToBeAdded products){
       return productsToBeAddedRepository.save(products);
    }
}
