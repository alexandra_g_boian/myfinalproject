package myApplication.service;

import myApplication.dao.UserRepository;
import myApplication.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User insertUser(User user) {
        userRepository.save(user);
        return user;
    }

    public void deleteUserById(Integer id) {
        userRepository.deleteById(id);
    }

    public void updateUser(Integer id, User user) {
        userRepository.updateUser(id, user);
    }

    public Iterable<User> getUsersByRole(String role) {
        Iterable<User> all = userRepository.findAll();
        List<User> userList = new ArrayList<>();
        for (User user : all) {
            if (user.getRole().equals(role)) {
                userList.add(user);
            }
        }
        return userList;
    }
}
